@include('front.partial.master')
<!DOCTYPE HTML>
<html class="greybody">
  @yield('jobshead')
  <link rel="stylesheet" href="{{URL::asset('assets/frontend/css/lightslider.min.css')}}">
  <body class="greybody jobdescbody">
    @yield('navbar')
    <div class="container bungkus" style="background:white;">
    <div class="page-header">
      <h1>Soal Ujian <small>{{$ujian->nama}}</small></h1>
    </div>
      
      <ol>
        @foreach($ujian->soalujians->shuffle() as $soal)
        <li>{{$soal->soal}}
            @foreach($soal->jawabanujians->shuffle() as $jawaban)
              <div class="radio">
                <label>
                  <input type="radio" name="jawabansoal{{$soal->id}}" id="inputJawabansoal" value="{{$jawaban->correct}}" required>
                  {{$jawaban->jawaban}}
                </label>
              </div>
            @endforeach
        </li>
        <hr>
        @endforeach
      </ol>
    
    <div class="checkbox">
      <label style="color:red;">
        <input type="checkbox" value="" required>
        Saya mengerjakan ujian ini dengan jujur dan tanpa bantuan orang lain.
      </label>
    </div>
    <div class="panel panel-default">
        <div class="panel-footer">
          <a type="button" class="btn btn-primary" href="{{URL::route('submitujian')}}" style="width:100%;">Submit Ujian</a>
        </div>
    </div>
    </div>

    <script src="{{URL::asset('assets/frontend/js/lightslider.js')}}" type="text/javascript"></script>
    <script type="text/javascript">
  $(document).ready(function() {
    $("#lightSlider").lightSlider({
    gallery: true,
    item: 1,
    loop: true,
    slideMargin: 0,
    thumbItem: 9
    }); 
  });
</script>
    @yield('footer')
  </body>
</html>