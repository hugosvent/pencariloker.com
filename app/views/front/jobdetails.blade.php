@include('front.partial.master')
<!DOCTYPE HTML>
<html class="greybody">
  @yield('jobshead')
  <link rel="stylesheet" href="{{URL::asset('assets/frontend/css/lightslider.min.css')}}">
  <body class="greybody jobdescbody">
    @yield('navbar')
    <div class="container bungkus">
      <div class="colputih">
<!-- 
        <!- - Carousel - ->
        <div id="myCarousel" class="carousel slide hidden-xs hidden-sm" data-ride="carousel">
          <!- - Indicators - ->
          <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
            <li data-target="#myCarousel" data-slide-to="3"></li>
          </ol>
          <!- - Wrapper for slides - ->
          <div class="carousel-inner" role="listbox">
            <div class="item active">
              <center><img id="img_ee9f_0"src="http://www.autocareersaz.com/sites/all/themes/autocareers/images/static-banner.jpg" alt="Chania"></center>
            </div>
            <div class="item">
              <center><img id="img_ee9f_1"src="http://www.yoursurreyjobs.co.uk/media/1062/time-for-change-web-banner-01.jpg" alt="Chania"></center>
            </div>
            <div class="item">
              <center><img id="img_ee9f_2"src="http://www.aapp.com.au/wp-content/uploads/ninja-forms/banner-002.jpg" alt="Flower"></center>
            </div>
            <div class="item">
              <center><img id="img_ee9f_3"src="https://www.eusa.ed.ac.uk/pageassets/jobs/EUSAJOBS_HEADER(1).jpg" alt="Flower"></center>
            </div>
          </div>
          <!- - Left and right controls - ->
          <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
        <!- - Carousel End - ->
 -->
        <hr style="margin-top:3px;">
        <div class="row smalldetail colputih">
          <div class="colputih col-md-6 col-xs-12">
            @if(!empty($lowongan->company->logo))
            <div class="col-md-4 col-xs-12 divdetaillogo">
              <img src="{{URL::asset($lowongan->company->logo)}}" class="jobdetaillogo img-responsive" alt="">
              <hr class="visible-sm visible-xs">
            </div>
            @endif
            <div class="colputih col-md-8 col-xs-12">
              <h2>{{$lowongan->name}}</h2>
              {{$lowongan->company->name}}
            </div>
          </div>
          <div class="colputih col-md-4 col-xs-12 pull-right">
            <ul class ="detailsmalllist" type="none">
              <li class="salary"><span><i class="fa fa-money"></i></span> {{$lowongan->gaji}}</li>
              <li><span><i class="fa fa-briefcase"></i></span> {{$lowongan->syaratpengalaman}}</li>
              <li><span style="padding:3px"><i class="fa fa-map-marker"></i></span> {{$lowongan->kotaprovinsi}}</li>
            </ul>
          </div>
        </div>
      </div>
      <hr>
      <div class="clearfix">
      
      </div>

      <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 kotakjobdesc">
              <div class="colputih jobdesc"><h4><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Deskripsi Lowongan</h4>
              <hr style="margin-top:1px;">
              <div class="unselectable wrap-text" id="job_description">
                {{$lowongan->descript}}
              </div>
              </div>
            </div>
            @if(!empty($lowongan->gmaps))
            <div class="col-xs-12 col-lg-12 col-md-12 col-sm-12 kotakjobdesc">
              <div class="colputih jobdesc">
                <h4><i class="fa fa-map-marker icon_header"></i> WORK LOCATION</h4>
                <hr style="margin-top:1px;">
                <center>
                <iframe src="{{$lowongan->gmaps}}"  frameborder="0" id="iframe_ee9f_0" allowfullscreen></iframe>
                </center>
              </div>
              <div class="clearfix">
              </div>
            </div>
            @endif
      </div>

      <div class="col-xs-12 col-lg-6 col-md-6 col-sm-6 kotakjobdesc">
        <div class="colputih jobdesc"><h4><i class="fa fa-list-alt icon_header"></i> Info Penyedia</h4>
        <hr style="margin-top:1px;">
          <div class="col-lg-6 col-md-6 col-sm-12">
            <p class="desc_subject">Nama Penyedia</p>
            <p>
              <span id="company_registration_number">{{$lowongan->company->name}}</span>
            </p>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-12">
            <p class="desc_subject">Industri</p>
            <p id="company_industry">{{$lowongan->company->industry}}</p>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-12">
            <p class="desc_subject">Jumlah Karyawan</p>
            <p id="company_size">{{$lowongan->company->size}} orang</p>
          </div>
          @if(!empty($lowongan->company->website))
          <div class="col-lg-6 col-md-6 col-sm-12">
            <p class="desc_subject">Website</p>
            <p>
              <a id="company_website" target="_blank" href="http://{{$lowongan->company->website}}">{{$lowongan->company->website}}</a>
            </p>
          </div>
          @endif
          @if(!empty($lowongan->company->phone))
          <div class="col-lg-6 col-md-6 col-sm-12">
            <p class="desc_subject">Telepon</p>
            <p id="company_contact">021-29809200</p>
          </div>
          @endif
          @if(!empty($lowongan->company->email))
          <div class="col-lg-6 col-md-6 col-sm-12">
            <p class="desc_subject">Email</p>
            <p id="work_environment_working_hours">{{$lowongan->company->email}}</p>
          </div>
          @endif
          @if(!empty($lowongan->company->address))
          <div class="col-lg-6 col-md-6 col-sm-12">
            <p class="desc_subject">Alamat</p>
            <p id="work_environment_working_hours">{{$lowongan->company->address}}</p>
          </div>
          @endif
        
          <div class="clearfix">
            
          </div>
        </div>
        <div class="clearfix">
          
        </div>
      </div>




@if(!empty($arr))
<div class="col-xs-12 col-lg-6 col-md-6 col-sm-6 kotakjobdesc">
  <div class="colputih jobdesc"><h4><i class="fa fa-list-alt icon_header"></i> COMPANY PHOTOS</h4>
  <hr style="margin-top:1px;">
    <ul id="lightSlider">
      @foreach($arr as $gambar)
      <li data-thumb="{{URL::asset($gambar)}}">
        <img class="img-responsive" src="{{URL::asset($gambar)}}" />
      </li>
      @endforeach
    </ul>
  </div>
  <div class="clearfix">
  </div>
</div>
@endif


<div class="clearfix">
  </div>
<div class="row smalldetail colputih" id="div_ee9f_7">
  <div class="colputih col-md-10 col-xs-12">
    <h4>Tanggal Pemasangan : {{date('d M Y',strtotime($lowongan->updated_at))}} </h4>
    <h4>Tanggal Berakhir : {{date('d M Y',strtotime($lowongan->tanggalberakhir))}} </h4>
  </div>
  <div class="colputih col-md-2 col-xs-12 pull-right">
      <center>
       <a class="btn btn-primary" id="button_ee9f_0" data-toggle="modal" href='#modal-id'>Apply Now!</a>
      </center>


      <div class="modal fade" id="modal-id">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title">Ujian</h4>
            </div>
            <div class="modal-body">
            <ul>
              @foreach($ujians as $ujian)
                <li><a href="{{URL::route('ujian', $ujian->id)}}">{{$ujian->nama}}</a></li>
              @endforeach
            </ul>
            </div>
<!--             <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary">Save changes</button>
            </div> -->
          </div>
        </div>
      </div>
  </div>
</div>

    </div>

    <script src="{{URL::asset('assets/frontend/js/lightslider.js')}}" type="text/javascript"></script>
    <script type="text/javascript">
  $(document).ready(function() {
    $("#lightSlider").lightSlider({
    gallery: true,
    item: 1,
    loop: true,
    slideMargin: 0,
    thumbItem: 9
    }); 
  });
</script>
    @yield('footer')
  </body>
</html>