@include('front.partial.master')
<!DOCTYPE HTML>
<html class="greybody">
  @yield('jobshead')
  <link rel="stylesheet" href="{{URL::asset('assets/frontend/css/lightslider.min.css')}}">
  <body class="greybody jobdescbody">
    @yield('navbar')


<div class="container content">

				{{ Form::open(array('url' => 'user'.'/'.$userprofile->id, 'files'=>true)) }}

					<div class="col-lg-12 col-xs-12 col-sm-12 col-md-6" style="margin:0 auto;">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3>Edit Profile {{$userprofile->name}}</h3>
							</div>
							<div class="panel-body">
								<div class="form-group">

									<img style="margin:0 auto;height:300px;width:auto;" @if(!is_null($userprofile->photos)) src="{{URL::asset($userprofile->photos)}}"  @else src="http://www.hdi-slc.com/wp-content/uploads/2012/07/blank-profile.jpg" @endif class="img-responsive" alt="Image">
									<input style="margin:0 auto;" type="file" accept="image/*" name="fotouser" value="" placeholder="">
								</div>
								<div class="form-group">
									<label>Nama</label>
									<input placeholder="Nama Kategori" name="name" value="{{$userprofile->name}}" class="inputkecil form-control">
								</div>
								<div class="form-group">
									<label>Username</label>
									<p class="inputkecil form-control" style="background:#DDD;">{{$userprofile->username}}</p>
								</div>
								<div class="form-group">
									<label>Email</label>
									<p class="inputkecil form-control" style="background:#DDD;">{{$userprofile->email}}</p>
								</div>
								<div class="form-group">
									<label>Gender</label>
									<select name="gender" id="input" class="form-control" required="required">
										<option @if($userprofile->gender == 0) selected @endif value="0">Pria</option>
										<option @if($userprofile->gender == 1) selected @endif value="1">Wanita</option>
									</select>
								</div>
								<div class="form-group">
									<label>Alamat</label>
									<textarea name="alamat" id="input" class="form-control" rows="3" required="required">{{$userprofile->address}}</textarea>
								</div>
								<div class="form-group">
									<label>Tanggal Lahir</label>
									<input style="width:160px;" type="date" name="dob" id="tanggal" class="form-control" value="{{{ isset($userprofile->dob) ? date('Y-m-d',strtotime($userprofile->dob)) : ''}}}" required="required" title="">
								</div>
								<div class="form-group">
									<label>No Telephone / Handphone</label>
									<input type="text" name="telepon" id="tanggal" class="form-control" value="{{$userprofile->phone}}" required="required" title="">
								</div>
								<div class="form-group">
									<label>File Lampiran ( CV / Resume / Riwayat hidup ) dalam bentuk .pdf</label>
									@if(!is_null($userprofile->lampiran))
									<a target="_blank" href="{{URL::asset($userprofile->lampiran)}}">Download Lampiran</a>
									@endif
									<input type="file" name="lampiran" accept=".pdf" value="" placeholder="">
								</div>
								<button type="submit" class="btn btn-primary">Submit</button>
							</div>
							<!-- /.panel-body -->
						</div>
						<!-- /.panel -->
					</div>
		
		            <!-- /.row -->
		        {{Form::close()}}


</div>		
     @yield('footer')
  </body>
</html>