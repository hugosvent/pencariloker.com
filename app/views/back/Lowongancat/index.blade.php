<!DOCTYPE html>
<html lang="en">

@include('back.partial.master')

@yield('head')

<body>

    <div id="wrapper">

     @yield('navbar')


        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Kategori Lowongan</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           List Kategori Lowongan
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Nama Kategori</th>
                                            <th style="width:350px;">Status (Click to toogle active/deactivate)</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($lowongancats as $lowongancat)
                                        <tr class="gradeU">
                                            <td>{{$lowongancat->name}}</td>
                                            <td class="center">
                                                @if($lowongancat->active == 1)
                                                <a style="text-decoration:none;"><span class="label label-success">Active</span></a>
                                                @else
                                                <a style="text-decoration:none;"><span class="label label-danger">Not-Active</span></a>
                                                @endif
                                                <a style="text-decoration:none;" href="{{action('LowongancatController@edit', $lowongancat->id)}}"><span class="label label-info">Edit</span></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->

                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
            
            
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
    });
    </script>

</body>

</html>
