<!DOCTYPE html>
<html lang="en">

@include('back.partial.master')

@yield('head')
<style> 
.inputkecil{
    width:100%;
}
</style>
<body>

    <div id="wrapper">

     @yield('navbar')

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Kategori Lowongan</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
            {{ Form::open(['action' => ['LowongancatController@update',$lowongancat->id],'method'=>'patch','role'=>'form']) }}
                <div class="col-lg-3">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Form Kategori Lowongan
                        </div>
                        <div class="panel-body">
                             <div class="form-group">
                                    <label>Nama Kategori</label>
                                    <input placeholder="Nama Kategori" name="nama" value="{{$lowongancat->name}}" class="inputkecil form-control">
                            </div>
                            <div class="form-group">
                                    <label>Status</label>
                                       <select name="status" id="input" class="form-control" required="required">
                                           <option value="1" selected>active</option>
                                           <option value="0">non-active</option>
                                       </select>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>

            <!-- /.row -->
        </div>
        {{Form::close()}}
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="{{ URL::asset('bower_components/jquery/dist/jquery.min.js') }}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{ URL::asset('bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="{{ URL::asset('bower_components/metisMenu/dist/metisMenu.min.js') }}"></script>

    <!-- Custom Theme JavaScript -->
    <script src="{{ URL::asset('dist/js/sb-admin-2.js') }}"></script>

</body>

</html>
