<!DOCTYPE html>
<html lang="en">

@include('back.partial.master')

@yield('head')

<body>

    <div id="wrapper">

     @yield('navbar')


        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">User</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           List User
                        </div>
                        <!-- /.panel-heading -->
                        
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th style="width:15%;">Nama</th>
                                            <th style="width:10%;">Username</th>
                                            <th style="width:10%;">Email</th>
                                            <th style="width:20%;">Status (Click to toogle active/deactivate)</th>
                                            <th style="width:25%;">Admin Privilage (Click to toogle active/deactivate)</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($listuser as $user)
                                        <tr class="gradeU">
                                            <td>{{$user->name}}</td>
                                            <td>{{$user->username}}</td>
                                            <td>{{$user->email}}</td>
                                            <td class="center">
                                                @if($user->active == 1)
                                                <a style="text-decoration:none;" href="{{action('UserController@activation', $user->id)}}"><span class="label label-success">Active</span></a>
                                                @else
                                                <a style="text-decoration:none;" href="{{action('UserController@activation', $user->id)}}"><span class="label label-danger">Not-Active</span></a>
                                                @endif
                                            </td>
                                            <td class="center">
                                                @if($user->admin == 1)
                                                <a style="text-decoration:none;" href="{{action('UserController@makeadmin', $user->id)}}"><span class="label label-success">Active</span></a>
                                                @else
                                                <a style="text-decoration:none;" href="{{action('UserController@makeadmin', $user->id)}}"><span class="label label-danger">Not-Active</span></a>
                                                @endif
                                            </td>

                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->

                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
            
            
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
    });
    </script>

</body>

</html>
