<!DOCTYPE html>
<html lang="en">

@include('back.partial.master')

@yield('head')
<style> 
.inputkecil{
    width:100%;
}
</style>
<body>

    <div id="wrapper">

     @yield('navbar')

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Forms</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-3">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Basic Form Elements
                        </div>
                        <div class="panel-body">
                             <div class="form-group">
                                    <label>Text Input</label>
                                    <input placeholder="input1" class="inputkecil form-control">
                            </div>
                            <div class="form-group">
                                    <label>Text Input</label>
                                    <input placeholder="input1" class="inputkecil form-control">
                            </div>
                            <div class="form-group">
                                    <label>Text Input</label>
                                    <input placeholder="input1" class="inputkecil form-control">
                            </div>
                            <div class="form-group">
                                    <label>Text Input</label>
                                    <input placeholder="input1" class="inputkecil form-control">
                            </div>
                            <div class="form-group">
                                    <label>Text Input</label>
                                    <input placeholder="input1" class="inputkecil form-control">
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>

            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="dist/js/sb-admin-2.js"></script>

</body>

</html>
