<!DOCTYPE html>
<html lang="en">

@include('back.partial.master')

@yield('head')

<body>

    <div id="wrapper">

     @yield('navbar')


        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Ujian Lowongan {{Lowongan::find($idlowongan)->name}}</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>

            <!-- Button trigger modal -->
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
            Tambah Ujian
            </button>
            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content" style="width:20em;">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Masukkan Jumlah Soal</h4>
                        </div>
                        <div class="modal-body">
                            {{ Form::open(['action' => 'UjianController@create']) }}
                            <input type="text" class="hidden" name="idlowongan" value="{{$idlowongan}}">
                            <input type="text" name="namaujian" id="inputNamaujian" class="form-control" value="" placeholder="Nama Ujian" required="required" title="">
                            <input type="number" name="jumlahsoal" id="input" class="form-control" value="5" min="5" max="20" step="1" required="required" title="">
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary" style="width:100%" formtarget="_blank">Tambah</button>
                             {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>

            <!-- /.row -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Daftar Ujian Lowongan {{Lowongan::find($idlowongan)->name}}
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">
                                    <div class="dataTable_wrapper">
                                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                            <thead>
                                                <tr>
                                                    <th>Jenis Ujian</th>
                                                    <th>Jumlah Soal</th>
                                                    <th>Persentase Nilai</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($listujian as $ujian)
                                                <tr class="odd gradeX">
                                                    <td>{{$ujian->nama}}</td>
                                                    <td>{{count($ujian->soalujians)}}</td>
                                                    <td>{{$ujian->percentage}} %</td>
                                                    <td>
                                                        {{ Form::open(['style'=>'float:left; margin:0 5px;' ,'action' => ['UjianController@destroy',$ujian->id], 'method' => 'delete']) }}
                                                        <button type="button" style="padding:0 5px;" class="btn btn-danger" data-toggle="modal" data-target="#hapus{{$ujian->id}}">
                                                        Hapus
                                                        </button>
                                                        <div class="modal fade" id="hapus{{$ujian->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content" style="width:20em;">
                                                                    <div class="panel panel-default">
                                                                        <div class="panel-heading">
                                                                            <h3 class="panel-title">Yakin ingin menghapus Ujian {{$ujian->nama}}?</h3>
                                                                        </div>
                                                                        <div class="panel-body">
                                                                            <button type="submit" style="width:100%;height:100%;" class="btn btn-danger">Hapus</button>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                        {{Form::close()}}
                                                        <a type="button" style="padding:0 5px;" target="_blank" class="btn btn-info" href="{{action('UjianController@edit', $ujian->id)}}">Edit</a>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.table-responsive -->
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
            <!-- /.row -->
            
            
            
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
    });
    </script>

</body>

</html>
