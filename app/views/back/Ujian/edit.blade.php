<!DOCTYPE html>
<html lang="en">

@include('back.partial.master')

@yield('head')
<style> 
.inputkecil{
    width:100%;
}
</style>
<body>

    <div id="wrapper">

     @yield('navbar')
 
  
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Tambah Ujian Lowongan</h1><br>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
            {{ Form::open(['action' => ['UjianController@update',$dataujian->id],'method' => 'patch','role'=>'form']) }}
    <input type="text" class="hidden" id="idlowongan" name="idlowongan" value="{{$dataujian->lowongan_id}}">
   <input type="text" class="hidden" id="idlowongan" name="jumlahsoal" value="{{count($dataujian->soalujians)}}">
   <input type="text" class="hidden" id="idlowongan" name="namaujian" value="{{$dataujian->nama}}">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Form Ujian Lowongan
                        </div>
                        <div class="panel-body">
                          <h2>Ujian {{$dataujian->nama}} Lowongan {{$dataujian->lowongan->name}}</h2>
                          <label for="percentage" style="color:red;">Persentase Bobot Ujian</label>
                          <input type="number" style="width:5em;" name="percentage" id="inputPercentage" class="form-control" value="{{$dataujian->percentage}}" min="10" max="100" step="1" required="required" title="">
                          <br><br>
                          <ol>
                          @for($i=1;$i<=count($dataujian->soalujians);$i++)
                          <li>
                          <textarea required style="margin-bottom:-0.5em; width:100%;" name="soal{{$i}}" id="" style="width:100%;" placeholder="Masukkan soal no {{$i}}" rows="1">{{$dataujian->soalujians[$i-1]->soal}}</textarea>
                            <div class="form-group soal" style="margin-top:1em">
                              <ul class="list-group">
                                <li class="list-group-item"><input type="text" name="jawaban_1_soal{{$i}}" value="{{$dataujian->soalujians[$i-1]->jawabanujians[0]->jawaban}}" placeholder="" required> <- jawaban nomor {{$i}}</li>
                                <li class="list-group-item"><input type="text" name="jawaban_2_soal{{$i}}" value="{{$dataujian->soalujians[$i-1]->jawabanujians[1]->jawaban}}" placeholder="" required></li>
                                <li class="list-group-item"><input type="text" name="jawaban_3_soal{{$i}}" value="{{$dataujian->soalujians[$i-1]->jawabanujians[2]->jawaban}}" placeholder="" required></li>
                                <li class="list-group-item"><input type="text" name="jawaban_4_soal{{$i}}" value="{{$dataujian->soalujians[$i-1]->jawabanujians[3]->jawaban}}" placeholder="" required></li>
                              </ul>
                            </div>
                          </li>
                          <br>
                          @endfor
                          </ol>
                          

                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>

            <!-- /.row -->
        </div>
        {{Form::close()}}
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
    <script src="//cdn.ckeditor.com/4.5.5/basic/ckeditor.js"></script>

    <!-- jQuery -->
    <script src="{{ URL::asset('bower_components/jquery/dist/jquery.min.js') }}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{ URL::asset('bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="{{ URL::asset('bower_components/metisMenu/dist/metisMenu.min.js') }}"></script>

    <!-- Custom Theme JavaScript -->
    <script src="{{ URL::asset('dist/js/sb-admin-2.js') }}"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

</body>

</html>
