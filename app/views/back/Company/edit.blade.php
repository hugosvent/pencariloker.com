<!DOCTYPE html>
<html lang="en">

@include('back.partial.master')

@yield('head')
<style> 
.inputkecil{
    width:100%;
}
</style>
<body>

    <div id="wrapper">

     @yield('navbar')

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Edit Penyedia Lowongan</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
            {{ Form::open(['action' => array('CompanyController@update',$company->id),'method'=>'patch','role'=>'form','files'=>true]) }}
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Form Penyedia Lowongan
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                    <label>Nama</label>
                                      <input value="{{$company->name}}" type="text" name="nama" id="input" class="form-control" required>
                            </div>
                            @if(Session::get('imagesizerestrict') != '')
                            <div class="alert alert-warning">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <strong>Logo tidak sesuai</strong> (Melebihi Ukuran)
                            </div>
                            @endif
                            <div class="form-group">
                                    <label>Logo</label> <span style="font-size:0.8em;">( Ukuran maksimal : 145px x 50px )</span>
                                    <input type="file" name="logo">
                                    <input type="text" class="hidden" id="inputlogo" name="inputlogo" value="{{$company->logo}}">
                                    <img id="gambarlogo" src="{{URL::asset($company->logo)}}" alt=""> <button type="button" onclick="hapus()" class="btn btn-danger" style="padding:0 5px">hapus</button>
                            </div>
                             <div class="form-group">
                                    <label>Industri</label>
                                      <input value="{{$company->industry}}" type="text" name="industri" id="input" class="form-control" required>
                            </div>
                             <div class="form-group">
                                    <label>Website</label>
                                      <input value="{{$company->website}}" type="text" name="website" id="input" class="form-control">
                            </div>
                             <div class="form-group">
                                    <label>Telepon</label>
                                      <input value="{{$company->phone}}" type="text" name="phone" id="input" class="form-control">
                            </div>
                             <div class="form-group">
                                    <label>Email</label>
                                      <input value="{{$company->email}}" type="email" name="email" id="input" class="form-control">
                            </div>
                            <div class="form-group">
                                    <label>Jumlah Karyawan</label>
                                      <input value="{{$company->size}}" type="text" name="jumlahkaryawan" id="input" class="form-control" required>
                            </div>
                             <div class="form-group">
                                    <label>Alamat</label>
                                      <textarea type="text" name="address" id="input" class="form-control" required>{{$company->address}}</textarea>
                            </div>

                            <div class="form-group">
                                    <label>Status</label>
                                       <select name="status" id="input" class="form-control" required="required">
                                           <option value="1" selected>active</option>
                                           <option value="0">non-active</option>
                                       </select>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>

            <!-- /.row -->
        </div>
        {{Form::close()}}
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
    <script src="//cdn.ckeditor.com/4.5.5/basic/ckeditor.js"></script>

    <!-- jQuery -->
    <script src="{{ URL::asset('bower_components/jquery/dist/jquery.min.js') }}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{ URL::asset('bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="{{ URL::asset('bower_components/metisMenu/dist/metisMenu.min.js') }}"></script>

    <!-- Custom Theme JavaScript -->
    <script src="{{ URL::asset('dist/js/sb-admin-2.js') }}"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
     <script>

     function hapus(){
          $('#inputlogo').val('');
          $('#gambarlogo').remove();
   };

        CKEDITOR.replace( 'inputDescription' );
        $(function() {
    $( ".datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' });
  });
    </script>
</body>

</html>
