<!DOCTYPE html>
<html lang="en">

@include('back.partial.master')

@yield('head')

<body>

    <div id="wrapper">

     @yield('navbar')


        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Tables</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            DataTables Advanced Tables
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Nama Lowongan</th>
                                            <th>Penyedia</th>
                                            <th>Kategori</th>
                                            <th>Gaji</th>
                                            <th>Tanggal Pemasangan</th>
                                            <th>Tanggal Berakhir</th>
                                            <th>Ujian</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($lowongans as $lowongan)
                                        <tr class="odd gradeX">
                                            
                                            <td>{{$lowongan->name}}</td>
                                            <td>{{$companies->find($lowongan->company_id)->name}}</td>
                                            <td>{{$lowongancats->find($lowongan->lowongancat_id)->name}}</td>
                                            <td class="center">{{$lowongan->gaji}}</td>
                                            <td>{{date('d/m/Y',strtotime($lowongan->created_at))}}</td>
                                            <td>{{date('d/m/Y',strtotime($lowongan->tanggalberakhir))}}</td>
                                            <td><a href="{{action('UjianController@index', ['lowid' => $lowongan->id])}}" style="text-decoration:none;"><span class="label label-primary">Lihat Ujian</span></a></td>
                                            <td class="center">
                                                @if($lowongan->active == 1)
                                                <a style="text-decoration:none;"><span class="label label-success">Active</span></a>
                                                @else
                                                <a style="text-decoration:none;"><span class="label label-danger">Not-Active</span></a>
                                                @endif
                                                <a style="text-decoration:none;" href="{{action('LowonganController@edit', $lowongan->id)}}"><span class="label label-info">Edit</span></a>
                                            </td>
                                        </tr>
                                     @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->

                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
            
            
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    

    jQuery.extend( jQuery.fn.dataTableExt.oSort, {
"date-uk-pre": function ( a ) {
    var ukDatea = a.split('/');
    return (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
},

"date-uk-asc": function ( a, b ) {
    return ((a < b) ? -1 : ((a > b) ? 1 : 0));
},

"date-uk-desc": function ( a, b ) {
    return ((a < b) ? 1 : ((a > b) ? -1 : 0));
}
} );

$(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true,
                "aoColumns": [
            null,
            null,
            null,
            null,
            { "sType": "date-uk" },
            {"sType": "date-uk" },
            null,
            null
        ]
        });
    });

    </script>

</body>

</html>
