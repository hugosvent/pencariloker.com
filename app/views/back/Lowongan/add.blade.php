<!DOCTYPE html>
<html lang="en">

@include('back.partial.master')

@yield('head')
<style> 
.inputkecil{
    width:100%;
}
</style>
<body>

    <div id="wrapper">

     @yield('navbar')

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Tambah Lowongan</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
            {{ Form::open(['action' => 'LowonganController@store','role'=>'form','files'=>true]) }}
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Form Lowongan
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                    <label>Nama Lowongan</label>
                                      <input type="text" name="nama" id="input" class="form-control" required>
                            </div>

                            <div class="form-group">
                                    <label>Kategori Lowongan</label>
                                       <select name="kategori" name="kategori" id="input" class="form-control" required="required">
                                          @foreach($lowongancats as $lowongancat)
                                            <option value="{{$lowongancat->id}}">{{$lowongancat->name}}</option>
                                            @endforeach
                                       </select>
                            </div>
                            <div class="form-group">
                                    <label>Penyedia Lowongan</label>
                                       <select name="penyedia" id="input" class="form-control">
                                          @foreach($companies as $company)
                                            <option value="{{$company->id}}">{{$company->name}}</option>
                                            @endforeach
                                       </select>
                            </div>
                            <div class="form-group">
                                    <label>Highlight <span style="font-weight:100;">(pisahkan dengan enter)</span></label>
                                       <textarea name="highlight" id="inputhighlight" class="form-control" rows="3" required="required"></textarea>
                            </div>
                            <div class="form-group">
                                    <label>Deskripsi Lowongan</label>
                                       <textarea name="description" id="inputDescription" class="form-control" rows="3" required="required"></textarea>
                            </div>
                             <div class="form-group">
                                    <label>Link Google Maps</label>
                                      <input type="text" name="gmaps" id="input" class="form-control">
                            </div>
                             <div class="form-group">
                                    <label>Gaji</label>
                                      <input type="text" name="gaji" id="input" class="form-control" required>
                            </div>
                             <div class="form-group">
                                    <label>Syarat Pengalaman</label>
                                      <input type="text" name="syarat" id="input" class="form-control" required>
                            </div>
                             <div class="form-group">
                                    <label>Kota Provinsi</label>
                                      <input type="text" name="kota" id="input" class="form-control" required>
                            </div>
                             <div class="form-group">
                                    <label>Tanggal berakhir</label>
                                      <input type="text" name="tanggalakhir" id="inputdate" class="form-control datepicker">
                            </div>
                            <div class="form-group" style="display:inline;float:left;">
                                    <label>Masukkan Gambar 1</label>
                                    <input type="file" name="gambar0">
                            </div>
                            <div class="form-group" style="display:inline;float:left;">
                                    <label>Masukkan Gambar 2</label>
                                    <input type="file" name="gambar1">
                            </div>
                            <div class="form-group" style="display:inline;float:left;">
                                    <label>Masukkan Gambar 3</label>
                                    <input type="file" name="gambar2">
                            </div>
                            <div class="form-group" style="display:inline;float:left;">
                                    <label>Masukkan Gambar 4</label>
                                    <input type="file" name="gambar3">
                            </div>
                            <div class="form-group" style="display:inline;float:left;">
                                    <label>Masukkan Gambar 5</label>
                                    <input type="file" name="gambar4">
                            </div>
                            <div class="clearfix">
                            
                            </div>
    
                            <div class="form-group">
                                    <label>Status</label>
                                       <select name="status" id="input" class="form-control" required="required">
                                           <option value="1" selected>active</option>
                                           <option value="0">non-active</option>
                                       </select>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>

            <!-- /.row -->
        </div>
        {{Form::close()}}
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
    <script src="//cdn.ckeditor.com/4.5.5/basic/ckeditor.js"></script>

    <!-- jQuery -->
    <script src="{{ URL::asset('bower_components/jquery/dist/jquery.min.js') }}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{ URL::asset('bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="{{ URL::asset('bower_components/metisMenu/dist/metisMenu.min.js') }}"></script>

    <!-- Custom Theme JavaScript -->
    <script src="{{ URL::asset('dist/js/sb-admin-2.js') }}"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
     <script>
        CKEDITOR.replace( 'inputDescription' );
        $(function() {
    $( ".datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' });
  });
    </script>
</body>

</html>
