<!DOCTYPE html>
<html lang="en">

@include('back.partial.master')

@yield('head')
<style> 
.inputkecil{
    width:100%;
}
</style>
<body>

    <div id="wrapper">

     @yield('navbar')

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Edit Lowongan</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
            {{ Form::open(['action' => array('LowonganController@update',$lowongan->id),'method'=>'patch','role'=>'form','files'=>true]) }}
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Form Lowongan
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                    <label>Nama Lowongan</label>
                                      <input type="text" name="nama" value="{{$lowongan->name}}" id="input" class="form-control" required>
                            </div>

                            <div class="form-group">
                                    <label>Kategori Lowongan</label>
                                       <select name="kategori" name="kategori" id="input" class="form-control" required="required">
                                          @foreach($lowongancats as $lowongancat)
                                            <option @if($lowongan->lowongancat->name == $lowongancat->name) selected @endif value="{{$lowongancat->id}}">{{$lowongancat->name}}</option>
                                            @endforeach
                                       </select>
                            </div>
                            <div class="form-group">
                                    <label>Penyedia Lowongan</label>
                                       <select name="penyedia" id="input" class="form-control">
                                          @foreach($companies as $company)
                                            <option @if($lowongan->company->name == $company->name) selected @endif value="{{$company->id}}">{{$company->name}}</option>
                                            @endforeach
                                       </select>
                            </div>
                            <div class="form-group">
                                    <label>Highlight <span style="font-weight:100;">(pisahkan dengan enter)</span></label>
                                       <textarea name="highlight" id="inputhighlight"  class="form-control" rows="3" required="required">{{str_replace(['<li>','</li>'],'',$lowongan->highlight)}}</textarea>
                            </div>
                            <div class="form-group">
                                    <label>Deskripsi Lowongan</label>
                                       <textarea name="description" id="inputDescription"  class="form-control" rows="3" required="required">{{$lowongan->descript}}</textarea>
                            </div>
                             <div class="form-group">
                                    <label>Link Google Maps</label>
                                      <input type="text" name="gmaps" id="input" value="{{$lowongan->gmaps}}" class="form-control">
                            </div>
                             <div class="form-group">
                                    <label>Gaji</label>
                                      <input type="text" name="gaji" id="input" value="{{$lowongan->gaji}}" class="form-control" required>
                            </div>
                             <div class="form-group">
                                    <label>Syarat Pengalaman</label>
                                      <input type="text" name="syarat" id="input" value="{{$lowongan->syaratpengalaman}}" class="form-control" required>
                            </div>
                             <div class="form-group">
                                    <label>Kota Provinsi</label>
                                      <input type="text" name="kota" id="input" value="{{$lowongan->kotaprovinsi}}" class="form-control" required>
                            </div>
                             <div class="form-group">
                                    <label>Tanggal berakhir</label>
                                      <input type="text" name="tanggalakhir" id="inputdate" value="{{date('Y-m-d',strtotime($lowongan->tanggalberakhir))}}" class="form-control datepicker">
                            </div>
                            <div class="form-group" style="display:inline;float:left;">
                                    <label>Masukkan Gambar 1</label><button type="button" id="hapusgambar0" style="margin-left:5px;padding:0 5px;" class="btn btn-danger">x</button>
                                    <input type="text" class="hidden" name="inputgambar0" id="inputgambar0" value="{{{$photos[0] or ''}}}" >
                                    <input type="file" name="gambar0" id="gambar0">
                                    <img id="img0" style="height:auto;width:100px;" @if(!empty($photos[0])) src="{{URL::asset($photos[0])}}" @endif alt="">
                            </div>
                            <div class="form-group" style="display:inline;float:left;">
                                    <label>Masukkan Gambar 2</label><button type="button" id="hapusgambar1" style="margin-left:5px;padding:0 5px;" class="btn btn-danger">x</button>
                                    <input type="text" class="hidden" name="inputgambar1" id="inputgambar1" value="{{{$photos[1] or ''}}}" >
                                    <input type="file" name="gambar1" id="gambar1">
                                    <img id="img1" style="height:auto;width:100px;" @if(!empty($photos[1])) src="{{URL::asset($photos[1])}}" @endif alt="">
                            </div>
                            <div class="form-group" style="display:inline;float:left;">
                                    <label>Masukkan Gambar 3</label><button type="button" id="hapusgambar2" style="margin-left:5px;padding:0 5px;" class="btn btn-danger">x</button>
                                    <input type="text" class="hidden" name="inputgambar2" id="inputgambar2" value="{{{$photos[2] or ''}}}" >
                                    <input type="file" name="gambar2" id="gambar2">
                                    <img id="img2" style="height:auto;width:100px;" @if(!empty($photos[2])) src="{{URL::asset($photos[2])}}" @endif alt="">
                            </div>
                            <div class="form-group" style="display:inline;float:left;">
                                    <label>Masukkan Gambar 4</label><button type="button" id="hapusgambar3" style="margin-left:5px;padding:0 5px;" class="btn btn-danger">x</button>
                                    <input type="text" class="hidden" name="inputgambar3" id="inputgambar3" value="{{{$photos[3] or ''}}}" >
                                    <input type="file" name="gambar3" id="gambar3">
                                    <img id="img3" style="height:auto;width:100px;" @if(!empty($photos[3])) src="{{URL::asset($photos[3])}}" @endif alt="">
                            </div>
                            <div class="form-group" style="display:inline;float:left;">
                                    <label>Masukkan Gambar 5</label><button type="button" id="hapusgambar4" style="margin-left:5px;padding:0 5px;" class="btn btn-danger">x</button>
                                    <input type="text" class="hidden" name="inputgambar4" id="inputgambar4" value="{{{$photos[4] or ''}}}" >
                                    <input type="file" name="gambar4" id="gambar4">
                                    <img id="img4" style="height:auto;width:100px;" @if(!empty($photos[4])) src="{{URL::asset($photos[4])}}" @endif alt="">
                            </div>
                            <div class="clearfix">
                            
                            </div>
    
                            <div class="form-group">
                                    <label>Status</label>
                                       <select name="status" id="input" class="form-control" required="required">
                                           <option value="1" selected>active</option>
                                           <option value="0">non-active</option>
                                       </select>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>

            <!-- /.row -->
        </div>
        {{Form::close()}}
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
    <script src="//cdn.ckeditor.com/4.5.5/basic/ckeditor.js"></script>

    <!-- jQuery -->
    <script src="{{ URL::asset('bower_components/jquery/dist/jquery.min.js') }}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{ URL::asset('bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="{{ URL::asset('bower_components/metisMenu/dist/metisMenu.min.js') }}"></script>

    <!-- Custom Theme JavaScript -->
    <script src="{{ URL::asset('dist/js/sb-admin-2.js') }}"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
     <script>
        CKEDITOR.replace( 'inputDescription' );
        $(function() {
    $( ".datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' });
  });

        $('#hapusgambar0').click(function(){
          $('#inputgambar0').val('');
          $('#img0').remove();
        });
         $('#hapusgambar1').click(function(){
          $('#inputgambar1').val('');
          $('#img1').remove();
        });
          $('#hapusgambar2').click(function(){
          $('#inputgambar2').val('');
          $('#img2').remove();
        });
           $('#hapusgambar3').click(function(){
          $('#inputgambar3').val('');
          $('#img3').remove();
        });
            $('#hapusgambar4').click(function(){
          $('#inputgambar4').val('');
          $('#img4').remove();
        });
    </script>
</body>

</html>
