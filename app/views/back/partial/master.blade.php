@section('head')
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Dashboard PencariLoker</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{ URL::asset('bower_components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="{{ URL::asset('bower_components/metisMenu/dist/metisMenu.min.css') }}" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="{{ URL::asset('dist/css/timeline.css') }}" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{ URL::asset('dist/css/sb-admin-2.css') }}" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="{{ URL::asset('bower_components/morrisjs/morris.css') }}" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="{{ URL::asset('bower_components/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
@stop

@section('navbar')
 <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#"><img src="{{ URL::asset('assets/logo.png')}}" class="img-responsive" style="height:120%;" id="logo" alt="Image"/></a>
            </div>
            <!-- /.navbar-header -->
            <ul class="nav navbar-top-links navbar-right">
                <!-- /.dropdown -->
                <div style="padding:20px 20px 10px 0;font-size:14px;font-weight:900"><a href="logout"><i class="fa fa-sign-out fa-fw"></i>Logout</a></div>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

<div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                    <!--Lamaran -->
                        <!-- <li>
                            <a href="#"><strong><i class="fa fa-file-text-o fa-fw"></i> Lamaran<span class="fa arrow"></span></strong></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="tables">Tambah Lamaran</a>
                                </li>
                                <li>
                                    <a href="tables">List Lamaran</a>
                                </li>
                            </ul>
                        </li> -->
                        <!-- Kategori Lowongan -->
                         <li>
                            <a href="#"><strong><i class="fa fa-newspaper-o fa-fw"></i> Kategori Lowongan<span class="fa arrow"></span></strong></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{action('LowongancatController@create')}}">Tambah Kategori</a>
                                </li>
                                <li>
                                    <a href="{{action('LowongancatController@index')}}">List Kategori</a>
                                </li>
                            </ul>
                        </li>
                        <!-- Job Vacancy -->
                        <li>
                            <a href="#"><strong><i class="fa fa-briefcase fa-fw"></i> Lowongan<span class="fa arrow"></span></strong></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{action('LowonganController@create')}}">Tambah Lowongan</a>
                                </li>
                                <li>
                                    <a href="{{action('LowonganController@index')}}">List Lowongan</a>
                                </li>
                            </ul>
                        </li>
                        <!-- Penyedia Lowongan -->
                        <li>
                            <a href="#"><strong><i class="fa fa-building-o fa-fw"></i> Penyedia Lowongan<span class="fa arrow"></span></strong></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{action('CompanyController@create')}}">Tambah Penyedia Lowongan</a>
                                </li>
                                <li>
                                    <a href="{{action('CompanyController@index')}}">List Penyedia Lowongan</a>
                                </li>
                            </ul>
                        </li>
                        <!-- Ujian -->
                       <!--  <li>
                            <a href="#"><strong><i class="fa fa-file-text-o fa-fw"></i> Ujian<span class="fa arrow"></span></strong></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="tables">Tambah Ujian</a>
                                </li>
                                <li>
                                    <a href="tables">List Ujian</a>
                                </li>
                            </ul>
                        </li> -->
                        <!-- User -->
                        <li>
                            <a href="#"><strong><i class="fa fa-users fa-fw"></i> User<span class="fa arrow"></span></strong></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{action('UserController@register')}}">Tambah User</a>
                                </li>
                                <li>
                                    <a href="{{action('UserCRUDController@index')}}">List User</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>
@stop