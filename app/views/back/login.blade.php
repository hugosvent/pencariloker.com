<!DOCTYPE html>
<html lang="en">

@include('back.partial.master')

@yield('head')

<body>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Please Sign In</h3>
                    </div>
                   <div class="panel-body">
                    {{ Form::open(array('url' => 'login')) }}
                    <p>
                                    {{ $errors->first('email') }}
                                    {{ $errors->first('password') }}
                    </p>
                    <p>
                                    {{ Form::label('email', 'Email') }}
                                    {{ Form::text('email', Input::old('email'), array('class' => 'form-control','placeholder'=>'Masukkan Email')) }}
                    </p>
                    <p>
                                    {{ Form::label('password', 'Password') }}
                                    {{ Form::password('password', array('class' => 'form-control','placeholder'=>'Masukkan Password')) }}
                    </p>
                    <p>{{ Form::submit('Login', array('class' => 'form-control btn btn-success btn-block')) }}</p>
                {{ Form::close() }}
                </div>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

</body>

</html>
