@include('front.partial.master')
<!DOCTYPE HTML>
<html>
@yield('jobshead')
<body>
@yield('navbar')

    <div class="container" style="margin-top:100px;">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Please Sign In</h3>
                    </div>
                   <div class="panel-body">
                    {{ Form::open(array('url' => 'register')) }}
                    <input type="text" style="display:none">
                    <input type="password" style="display:none">
                    <p>
                                    {{ $errors->first('email') }}
                                    {{ $errors->first('password') }}
                    </p>
                     <p>
                                    {{ Form::label('name', 'Name') }}
                                    {{ Form::text('name', Input::old('name'), array('class' => 'form-control','placeholder'=>'Masukkan Nama')) }}
                    </p>
                     <p>
                                    {{ Form::label('username', 'Username') }}
                                    {{ Form::text('username', Input::old('username'), array('class' => 'form-control','placeholder'=>'Masukkan Username')) }}
                    </p>
                    <p>
                                    {{ Form::label('email', 'Email') }}
                                    {{ Form::text('email', Input::old('email'), array('class' => 'form-control','placeholder'=>'Masukkan Email')) }}
                    </p>
                    <p>
                                    {{ Form::label('password', 'Password') }}
                                    {{ Form::password('password', array('class' => 'form-control','placeholder'=>'Masukkan Password')) }}
                    </p>
                    <p>{{ Form::submit('Register', array('class' => 'btn btn-success btn-block','style' => 'width:100%')) }}</p>
                    <a href="login">Sudah punya akun? Login disini</a>
                {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

</body>

</html>

 