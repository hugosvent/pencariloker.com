<?php


class Jawabanujian extends Eloquent {
	protected $table = 'jawaban_ujians';
	protected $guarded = ['id'];
	public $timestamps = false;

	  public function soalujian()
    {
        return $this->belongsTo('Soalujian','soal_id');
    }


}