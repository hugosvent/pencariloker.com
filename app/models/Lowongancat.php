<?php


class Lowongancat extends Eloquent {
	protected $table = 'lowongancats';
	protected $guarded = ['id'];
	public $timestamps = true;

	
	 public function lowongans()
    {
        return $this->hasMany('Lowongan');
    }

}