<?php


class Lowongan extends Eloquent {
	protected $table = 'lowongans';
	protected $guarded = ['id'];
	public $timestamps = true;

	  public function company()
    {
        return $this->belongsTo('Company');
    }
     public function lowongancat()
    {
        return $this->belongsTo('Lowongancat');
    }


}