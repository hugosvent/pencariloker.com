<?php


class Soalujian extends Eloquent {
	protected $table = 'soal_ujians';
	protected $guarded = ['id'];
	public $timestamps = false;

	  public function ujian()
    {
        return $this->belongsTo('Ujian');
    }
public function jawabanujians()
    {
        return $this->hasMany('Jawabanujian','soal_id');
    }

}