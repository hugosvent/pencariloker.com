<?php


class Ujian extends Eloquent {
	protected $table = 'ujians';
	protected $guarded = ['id'];
	public $timestamps = true;

	  public function lowongan()
    {
        return $this->belongsTo('Lowongan');
    }
    public function soalujians()
    {
        return $this->hasMany('Soalujian');
    }


}