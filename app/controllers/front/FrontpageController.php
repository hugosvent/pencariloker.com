<?php

class FrontpageController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function index()
	{

		return View::make('front.index');
	}
	
	public function jobs()
	{

		function truncate($string,$length,$append="&hellip;") {
		  $string = trim($string);

		  if(strlen($string) > $length) {
		    $string = wordwrap($string, $length);
		    $string = explode("\n", $string, 2);
		    $string = $string[0] . $append;
		  }

		  return $string;
		}


		return View::make('front.jobs')->with([
				'lowongans'=> Lowongan::paginate(8),
				'companies'=> Company::get(),
				'lowongancats'=> Lowongancat::get(),
			]);
	}

	public function search()
	{

		function truncate($string,$length,$append="&hellip;") {
		  $string = trim($string);

		  if(strlen($string) > $length) {
		    $string = wordwrap($string, $length);
		    $string = explode("\n", $string, 2);
		    $string = $string[0] . $append;
		  }

		  return $string;
		}


		return View::make('front.jobs')->with([
				'lowongans'=> Lowongan::where('company_id', '=',Input::get('idcomp') )->paginate(8),
				'companies'=> Company::get(),
				'lowongancats'=> Lowongancat::get(),
			]);
	}
	
	public function jobdetails($id)
	{
		$lowongan = Lowongan::find($id);
		$arr = explode("||",$lowongan->photos);

		
		return View::make('front.jobdetails')->with([
			  'lowongan' => $lowongan,
			  'arr' => $arr,
			  'ujians' => Ujian::where('lowongan_id','=',$id)->get(),
			]);
	}

	public function viewuserprofile($id)
	{
		if(!Auth::check()){
			return Response::make('Unauthorized', 401);
		}
		
		if(Auth::user()->id != $id){
			return Response::make('Unauthorized', 401);
		}

		return View::make('front.userprofile')->with([
			  'userprofile' => User::find($id),
			]);
	}

	public function userprofileupdate($id)
	{
		$user=User::find($id);
		if(!is_null(Input::file('fotouser'))){
				Input::file('fotouser')->move('assets/images/userphoto',$user->id.'-'.trim($user->name).'.'.Input::file('fotouser')->getClientOriginalExtension());
				$path = 'assets/images/userphoto/'.$user->id.'-'.trim($user->name).'.'.Input::file('fotouser')->getClientOriginalExtension();

			$user->update([
				'photos' => $path,
				]);
			}

		if(!is_null(Input::file('lampiran'))){
				Input::file('lampiran')->move('assets/lampiranuser',$user->id.'-'.trim($user->name).'.'.Input::file('lampiran')->getClientOriginalExtension());
				$pathlampiran = 'assets/lampiranuser/'.$user->id.'-'.trim($user->name).'.'.Input::file('lampiran')->getClientOriginalExtension();
				$user->update([
				'lampiran' => $pathlampiran,
				]);
			}

		$user->update([
			'name' => Input::get('name'),
			'gender' => Input::get('gender'),
			'address' => Input::get('alamat'),
			'dob' => Input::get('dob'),
			'phone' => Input::get('telepon'),
			]);

		return Redirect::back();
	}

	public function takeujian($id)
	{
		return View::make('front.ujianlowongan')->with([
			  'ujian' => Ujian::find($id),
			]);
	}

}
