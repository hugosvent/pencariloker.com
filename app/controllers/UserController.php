<?php
 
class UserController extends \BaseController {
 
    public function login()
    {
                    return View::make('login');
    }

    public function activation($id)
    {
        $user = User::find($id);

        if($user->active !=1){
          $user->active = 1;
        }
        else{
            $user->active=0;
        }
        $user->save();
        return Redirect::to('user');
    }

     public function makeadmin($id)
    {
        $user = User::find($id);

        if($user->admin !=1){
          $user->admin = 1;
        }
        else{
            $user->admin=0;
        }
        $user->save();
        return Redirect::to('user');
    }
 
    public function doLogin()
    {
        $rules = array(
                        'email'    => 'required',
                        'password' => 'required|alphaNum|min:5'
        );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
                        return Redirect::to('login')
                                        ->withErrors($validator)
                                        ->withInput(Input::except('password'));
        } else {
                        $userdata = array(
                                        'email'   => Input::get('email'),
                                        'password'          => Input::get('password')
                        );
                        if (Auth::attempt(array('email'=>$userdata['email'],'password'=>$userdata['password'],'active'=>1))) {
                                        return Redirect::intended();
                        } 
                        elseif (Auth::attempt(array('username'=>$userdata['email'],'password'=>$userdata['password'],'active'=>1))) {
                                        return Redirect::intended();
                        } else {               
                                        return Redirect::to('login')->with('error','Password dan user tidak sesuai');
                        }
        }
    }
 
 public function register(){
            return View::make('register');
    }

    public function doRegister(){
         $rules = array(
                        'email'    => 'required|email',
                        'password' => 'required|alphaNum|min:5'
        );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
                        return Redirect::to('register')
                                        ->withErrors($validator)
                                        ->withInput(Input::except('password'));
        } else {
            $User = User::create([
                'name' => Input::get('name'),
                'username' => Input::get('username'),
                'email' => Input::get('email'),
                'password' => Hash::make(Input::get('password')),
                'active' => 1,
                'admin' => 0,
            ]);
               return Redirect::to('login');        
        }

    }
    public function logout()
    {
        Auth::logout();
        return Redirect::to('');
    }
 
}