<?php

class LowongancatController extends \BaseController {
	public function __construct()
	{
		$this->beforeFilter('auth');
		
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return View::make('back.Lowongancat.index')->with([
				'lowongancats' => Lowongancat::get(),
			]);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('back.Lowongancat.add');
	}


	/**
	 * Store a newly created resource in storage.
	 *0000000000
	 * @return Response
	 */
	public function store()
	{
            $Lowongancat = Lowongancat::create([
                'name' => Input::get('nama'),
                'active' => Input::get('status'),
            ]);
            return View::make('back.Lowongancat.add');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{

	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		return View::make('back.Lowongancat.edit')->with([
			'lowongancat' => Lowongancat::find($id),
			]);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$Lowongancat = Lowongancat::find($id)->update([
                'name' => Input::get('nama'),
                'active' => Input::get('status'),
            ]);
            return Redirect::action('LowongancatController@index');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{	
		
	}
	

}
