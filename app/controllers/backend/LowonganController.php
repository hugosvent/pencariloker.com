<?php

class LowonganController extends \BaseController {
	public function __construct()
	{
		$this->beforeFilter('auth');
		
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return View::make('back.Lowongan.index')->with([
			'lowongancats'=>Lowongancat::get(),
			'companies'=>Company::get(),
			'lowongans'=>Lowongan::get(),
			]);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('back.Lowongan.add')->with([
			'lowongancats'=>Lowongancat::get(),
			'companies'=> Company::get(),
			]);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$array = array();
		for($i=0;$i<5;$i++){
			
			if(!is_null(Input::file('gambar'.$i))){
				Input::file('gambar'.$i)->move('assets/images',Input::file('gambar'.$i)->getClientOriginalName());
				$path = 'assets/images/'.Input::file('gambar'.$i)->getClientOriginalName();
				array_push($array,
						$path
					);
			}
		}
		$gmaps=Input::get('gmaps');
		if(!empty($gmaps)){
		preg_match('/<iframe.*src=\"(.*)\".*><\/iframe>/isU',Input::get('gmaps'), $matches);
		$gmaps = $matches[1];
		}
		// return $gmaps.' '.implode('||',$array);

		 $Lowongan = Lowongan::create([
                'lowongancat_id' => Input::get('kategori'),
                'company_id' => Input::get('penyedia'),
                'name' => Input::get('nama'),
                'descript' => Input::get('description'),
                'highlight' => '<li>'.str_replace(array("\r","\n\n","\n"),array('',"\n","</li>\n<li>"),trim(Input::get('highlight'),"\n\r")).'</li>',
                'gmaps' => $gmaps,
                'photos' => implode('||',$array),
                'gaji' => Input::get('gaji'),
                'syaratpengalaman' => Input::get('syarat'),
                'tanggalberakhir' => Input::get('tanggalakhir'),
                'kotaprovinsi' => Input::get('kota'),
                'active' => Input::get('status'),
            ]);
            return Redirect::action('LowonganController@index');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{

	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		return View::make('back.Lowongan.edit')->with([
			'photos'=> explode("||",Lowongan::find($id)->photos),
			'lowongan'=>Lowongan::find($id),
			'lowongancats'=>Lowongancat::get(),
			'companies'=> Company::get(),
			]);

	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$gmaps= Input::get('gmaps');
		if(!empty($gmaps)){
			if (strpos($gmaps,'iframe') !== false) {
				preg_match('/<iframe.*src=\"(.*)\".*><\/iframe>/isU',Input::get('gmaps'), $matches);
				$gmaps = $matches[1];
			}
		}

		$array = array();
		for($i=0;$i<5;$i++){
			
			if(!is_null(Input::file('gambar'.$i))){
				Input::file('gambar'.$i)->move('assets/images',Input::file('gambar'.$i)->getClientOriginalName());
				$path = 'assets/images/'.Input::file('gambar'.$i)->getClientOriginalName();
				array_push($array,
						$path
					);
			}
			elseif(Input::get('inputgambar'.$i) != ''){
				array_push($array,
						Input::get('inputgambar'.$i)
					);
			}
		}

		Lowongan::find($id)->update([
                'lowongancat_id' => Input::get('kategori'),
                'company_id' => Input::get('penyedia'),
                'name' => Input::get('nama'),
                'descript' => Input::get('description'),
                'highlight' => '<li>'.str_replace(array("\r","\n\n","\n"),array('',"\n","</li>\n<li>"),trim(Input::get('highlight'),"\n\r")).'</li>',
                'gmaps' => $gmaps,
                'photos' => implode('||',$array),
                'gaji' => Input::get('gaji'),
                'syaratpengalaman' => Input::get('syarat'),
                'tanggalberakhir' => Input::get('tanggalakhir'),
                'kotaprovinsi' => Input::get('kota'),
                'active' => Input::get('status'),
            ]);
            return Redirect::action('LowonganController@index');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{	
		
	}
	

}
