<?php

class CompanyController extends \BaseController {
	public function __construct()
	{
		$this->beforeFilter('auth');
		
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return View::make('back.Company.index')->with([
				'companies' => Company::get(),
			]);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('back.Company.add');
	}


	/**
	 * Store a newly created resource in storage.
	 *0000000000
	 * @return Response
	 */
	public function store()
	{
		if(!is_null(Input::file('logo'))){

			list($width, $height) = getimagesize(Input::file('logo'));

				
			
			

			if($width <= 145 && $height <= 50){
				Input::file('logo')->move('assets/images/logo/'.Input::get('nama'),Input::file('logo')->getClientOriginalName());
				$path = 'assets/images/logo/'.Input::get('nama').'/'.Input::file('logo')->getClientOriginalName();
            $company = Company::create([
                'name' => Input::get('nama'),
                'logo' => $path,
                'industry' => Input::get('industri'),
                'website' => Input::get('website'),
                'phone' => Input::get('phone'),
                'email'=> (!is_null(trim(Input::get('email'))) && trim(Input::get('email')) != "") ? preg_replace('#^https?://#', '', Input::get('email')) : null,
                'size' => Input::get('jumlahkaryawan'),
                'address' => Input::get('address'),
                'active' => Input::get('status'),
            ]);
            return View::make('back.Company.add');
        }
        else
        {
        	return View::make('back.Company.add')->with([
        			'imagesizerestrict'=> 'Logo tidak sesuai ( Melebihi ukuran )'
        		]);
        }

		}

		else{	
            $company = Company::create([
                'name' => Input::get('nama'),
                'industry' => Input::get('industri'),
                'website' => Input::get('website'),
                'phone' => Input::get('phone'),
                'email'=> (!is_null(trim(Input::get('email'))) && trim(Input::get('email')) != "") ? preg_replace('#^https?://#', '', Input::get('email')) : null,
                'size' => Input::get('jumlahkaryawan'),
                'address' => Input::get('address'),
                'active' => Input::get('status'),
            ]);
            return View::make('back.Company.add');
        }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{

	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		return View::make('back.Company.edit')->with([
				'company' => Company::find($id)
			]);	
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		if(!is_null(Input::file('logo'))){

			list($width, $height) = getimagesize(Input::file('logo'));

			if($width <= 145 && $height <= 50){
				Input::file('logo')->move('assets/images/logo/'.Input::get('nama'),Input::file('logo')->getClientOriginalName());
				$path = 'assets/images/logo/'.Input::get('nama').'/'.Input::file('logo')->getClientOriginalName();
            $company = Company::find($id)->update([
                'name' => Input::get('nama'),
                'logo' => $path,
                'industry' => Input::get('industri'),
                'website' => Input::get('website'),
                'phone' => Input::get('phone'),
                'email'=> (!is_null(trim(Input::get('email'))) && trim(Input::get('email')) != "") ? preg_replace('#^https?://#', '', Input::get('email')) : null,
                'size' => Input::get('jumlahkaryawan'),
                'address' => Input::get('address'),
                'active' => Input::get('status'),
            ]);
             return Redirect::action('CompanyController@index');
        }
        else
        {	
        	return  Redirect::back()
					->with('imagesizerestrict','Gambar Tidak Boleh Kosong')->withInput();
        }

		}
		elseif (Input::get('inputlogo') != '' ){
			$company = Company::find($id)->update([
                'name' => Input::get('nama'),
                'logo' => Input::get('inputlogo'),
                'industry' => Input::get('industri'),
                'website' => Input::get('website'),
                'phone' => Input::get('phone'),
                'email'=> (!is_null(trim(Input::get('email'))) && trim(Input::get('email')) != "") ? preg_replace('#^https?://#', '', Input::get('email')) : null,
                'size' => Input::get('jumlahkaryawan'),
                'address' => Input::get('address'),
                'active' => Input::get('status'),
            ]);
             return Redirect::action('CompanyController@index');
		}

		else{	
            $company = Company::find($id)->update([
                'name' => Input::get('nama'),
                'industry' => Input::get('industri'),
                'website' => Input::get('website'),
                'logo' => null,
                'phone' => Input::get('phone'),
                'email'=> (!is_null(trim(Input::get('email'))) && trim(Input::get('email')) != "") ? preg_replace('#^https?://#', '', Input::get('email')) : null,
                'size' => Input::get('jumlahkaryawan'),
                'address' => Input::get('address'),
                'active' => Input::get('status'),
            ]);
             return Redirect::action('CompanyController@index');
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{	
		
	}
	

}
