<?php

class UjianController extends \BaseController {
	public function __construct()
	{
		$this->beforeFilter('auth');
		
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return View::make('back.Ujian.index')->with([
				'idlowongan' => $_GET["lowid"],
				'listujian' => Ujian::where('lowongan_id', '=', $_GET["lowid"])->get(),
			]);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('back.Ujian.add')->with([
				'id_lowongan' => Input::get('idlowongan'),
				'jumlahsoal' => Input::get('jumlahsoal'),
				'namaujian' => Input::get('namaujian'),
			]);
	}


	/**
	 * Store a newly created resource in storage.
	 *0000000000
	 * @return Response
	 */
	public function store()
	{

		$ujian = Ujian::create([
			'lowongan_id' => Input::get('idlowongan'),
			'percentage' => Input::get('percentage'),
			'nama' => Input::get('namaujian'),
			]);

		for($i=1;$i<=Input::get('jumlahsoal');$i++){
			$soalujian = Soalujian::create([
				'ujian_id' => $ujian->id,
				'soal' => Input::get('soal'.$i),
				]);
			Jawabanujian::create([
				'soal_id' => $soalujian->id,
				'jawaban' => Input::get('jawaban_1_soal'.$i),
				'correct' => 1,
				]);
			for($j=2;$j<=4;$j++)
			Jawabanujian::create([
				'soal_id' => $soalujian->id,
				'jawaban' => Input::get('jawaban_'.$j.'_soal'.$i),
				'correct' => 0,
				]);
		}
		return View::make('back.Ujian.add')->with([
				'id_lowongan' => Input::get('idlowongan'),
				'jumlahsoal' => Input::get('jumlahsoal'),
				'namaujian' => Input::get('namaujian'),
			]);
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{

	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		return View::make('back.Ujian.edit')->with([
				'dataujian' => Ujian::find($id),
			]);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$hapus = Ujian::find($id);
		$hapus->delete();
		$ujian = Ujian::create([
			'lowongan_id' => Input::get('idlowongan'),
			'percentage' => Input::get('percentage'),
			'nama' => Input::get('namaujian'),
			]);

		for($i=1;$i<=Input::get('jumlahsoal');$i++){
			$soalujian = Soalujian::create([
				'ujian_id' => $ujian->id,
				'soal' => Input::get('soal'.$i),
				]);
			Jawabanujian::create([
				'soal_id' => $soalujian->id,
				'jawaban' => Input::get('jawaban_1_soal'.$i),
				'correct' => 1,
				]);
			for($j=2;$j<=4;$j++)
			Jawabanujian::create([
				'soal_id' => $soalujian->id,
				'jawaban' => Input::get('jawaban_'.$j.'_soal'.$i),
				'correct' => 0,
				]);
		}
		return View::make('back.Ujian.add')->with([
				'id_lowongan' => Input::get('idlowongan'),
				'jumlahsoal' => Input::get('jumlahsoal'),
				'namaujian' => Input::get('namaujian'),
			]);	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{	
		Ujian::destroy($id);
		return Redirect::back();
	}
	

}
