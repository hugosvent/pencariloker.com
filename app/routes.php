<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/




// frontend
Route::get('/', array('as' => 'online_home', 'uses' => 'FrontpageController@index'));
Route::get('/register', array('as' => 'register', 'uses' => 'UserController@register'));
Route::get('/login', array('as' => 'login', 'uses' => 'UserController@login'));
Route::get('/logout', array('as' => 'logout', 'uses' => 'UserController@logout'));

Route::get('/jobs', array('as' => 'jobs', 'uses' => 'FrontpageController@jobs'));

Route::get('/ujianlowongan/{id}', array('as' => 'ujian', 'uses' => 'FrontpageController@takeujian'));

Route::get('/submitujian', array('as' => 'submitujian', function()
{
	return View::make('front.fakeujian');
}));


Route::get('/jobdetails/{id}', array('as' => 'jobdetails', 'uses' => 'FrontpageController@jobdetails'));

Route::get('user/{id}', array('as' => 'profile', 'uses' => 'FrontpageController@viewuserprofile'));
Route::post('user/{id}', array('as' => 'profile', 'uses' => 'FrontpageController@userprofileupdate'));

// auth


Route::group(array('before' => 'auth'), function()
{
Route::get('/activation/{id}', ['uses' =>'UserController@activation']);
Route::get('/makeadmin/{id}', ['uses' =>'UserController@makeadmin']);
Route::get('logout', array('uses' => 'UserController@logout'));

Route::group(array('before'=>'admin'),function(){
// backend

// Route::get('ujian/create', array('as' => 'createujian', 'uses' => 'UjianController@create'));
Route::post('ujian/create', array('as' => 'createujian', 'uses' => 'UjianController@create'));

Route::resource('user', 'UserCRUDController');
Route::resource('lowongan', 'LowonganController');
Route::resource('lowongancat', 'LowongancatController');
Route::resource('company', 'CompanyController');
Route::resource('ujian', 'UjianController');
Route::get('/admin', function()
{
	return View::make('back.index');
});

// backend dev
Route::get('/tables', function()
{
	return View::make('back.tables');
});
Route::get('/forms', function()
{
	return View::make('back.forms');
});
 });


// Route yang ingin diproteksi taruh disini
});
 
Route::get('login', array('uses' => 'UserController@login'));
Route::post('login', array('uses' => 'UserController@doLogin'));

Route::get('register', array('uses' => 'UserController@register'));
Route::post('register', array('uses' => 'UserController@doRegister'));

Route::get('search', array('uses' => 'FrontpageController@search'));