<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUjiansTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		 Schema::create('ujians', function(Blueprint $table)
    {
        $table->increments('id');
        $table->integer('lowongan_id')->unsigned();
        $table->integer('percentage');
        $table->string('nama',100);
        $table->timestamps();

		$table->foreign('lowongan_id')
		      ->references('id')->on('lowongans')
		      ->onDelete('cascade')
		      ->onUpdate('cascade');

    });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('ujians');
	}

}
