<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLowongansTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
		public function up()
{
    Schema::create('lowongans', function(Blueprint $table)
    {
        $table->increments('id');
        $table->integer('lowongancat_id')->unsigned();
        $table->integer('company_id')->unsigned();
        $table->string('name', 100);
        $table->text('descript');
        $table->string('gmaps',255);
        $table->text('photos');
        $table->string('gaji',30);
        $table->string('syaratpengalaman',30);
        $table->string('kotaprovinsi',30);
        $table->timestamps();


    });
}
 
public function down()
{
    Schema::drop('lowongans');
}

}
