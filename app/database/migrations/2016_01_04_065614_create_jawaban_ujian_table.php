<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJawabanUjianTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// 
		Schema::create('jawaban_ujians', function(Blueprint $table)
    {
        $table->increments('id');
        $table->integer('soal_id')->unsigned();
        $table->text('jawaban');
        $table->tinyInteger('correct');

		$table->foreign('soal_id')
		      ->references('id')->on('soal_ujians')
		      ->onDelete('cascade')
		      ->onUpdate('cascade');

    });


	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('jawaban_ujians');
	}

}
