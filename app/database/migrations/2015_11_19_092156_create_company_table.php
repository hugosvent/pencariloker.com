<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
{
    Schema::create('companies', function(Blueprint $table)
    {
        $table->increments('id');
        $table->string('name', 100);
        $table->string('industry',150);
        $table->string('website',100);
        $table->string('phone',50);
        $table->string('email',40);
        $table->string('size',50);
        $table->string('address',200);
        $table->timestamps();


    });
}
 
public function down()
{
    Schema::drop('companies');
}

}
