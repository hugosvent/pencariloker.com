<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSoalUjiansTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		 Schema::create('soal_ujians', function(Blueprint $table)
    {
        $table->increments('id');
        $table->integer('ujian_id')->unsigned();
        $table->text('soal');

		$table->foreign('ujian_id')
		      ->references('id')->on('ujians')
		      ->onDelete('cascade')
		      ->onUpdate('cascade');

    });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('soal_ujians');
	}

}
