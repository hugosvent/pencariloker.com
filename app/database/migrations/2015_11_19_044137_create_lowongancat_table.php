<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLowongancatTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
{
    Schema::create('lowongancats', function(Blueprint $table)
    {
        $table->increments('id');
        $table->string('name', 50);
        $table->timestamps();
    });
}
 
public function down()
{
    Schema::drop('users');
}

}
